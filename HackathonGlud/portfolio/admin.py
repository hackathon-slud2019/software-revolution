from django.contrib import admin
from .models import Project

from django.utils.html import format_html_join
from django.utils.safestring import mark_safe

# Register your models here.

# Clase de configuración extendida
class ProjectAdmin(admin.ModelAdmin):
	readonly_fields = ('created', 'updated',)

admin.site.register(Project, ProjectAdmin)

