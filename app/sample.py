#import tensorflow as tf
#import tensorflow_hub as hub
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import re
from string import punctuation
#import nltk
#from nltk.corpus import stopwords
#from nltk import word_tokenize
#from nltk.data import load
#from nltk.stem import SnowballStemmer

#Imports from scikit-learn
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.model_selection import train_test_split 
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.metrics import confusion_matrix 

#Imports from keras for DNN Modeling
from keras.models import Sequential
from keras import layers

def load_data():
    data = pd.read_csv("data.csv", sep="|")
    #data_final= pd.read_csv("datos_completos.csv", sep="|")
    #print(data)
    data = data.rename(columns={"La renuncia de Germán Vargas Lleras en video https://t.co/51NbtH35hZ vía @RevistaSemana": "tweet"})
    #print(data.count())
    data = data.dropna(axis=0, how='any')
    data['polaridad']=pd.Series(np.random.randint(2), index=data.index)
    #print(data.count())
    for item in data.index:
        if((data['Polaridad'][item]=='Negativo')):
            data['polaridad'][item]=0
        elif ((data['Polaridad'][item]=='Neutral')):
            data['polaridad'][item]=1
        elif ((data['Polaridad'][item]=='Positivo')):
            data['polaridad'][item]=2            
    data['tweet'] = data['tweet'].map(lambda x: x.lower())
    return data
    #print(data["polaridad"].value_counts())
    #data["polaridad"].value_counts().plot(y="conteo de polaridad", kind='bar')
    
def vectorize_data(data):
    vectorizer = CountVectorizer(min_df=0, lowercase=False)
    vectorizer.fit(data)
    #print(vectorizer.vocabulary_)
    return vectorizer
    #textTokenized = []
    #for d in data['tweet']:
    #    textTokenized.append(vectorizer.transform([d]).toarray())
    #return textTokenized


def plot_history(history):
    acc = history.history['acc']
    val_acc = history.history['val_acc']
    loss = history.history['loss']
    val_loss = history.history['val_loss']
    x = range(1, len(acc) + 1)
    plt.figure(figsize=(12, 5))
    plt.subplot(1, 2, 1)
    plt.plot(x, acc, 'b', label='Precisión Entrenamiento')
    plt.plot(x, val_acc, 'r', label='Precisión Validación')
    plt.title('Precision Entrenamiento y validacion')
    plt.legend()
    plt.subplot(1, 2, 2)
    plt.plot(x, loss, 'b', label='Perdida Entrenamiento')
    plt.plot(x, val_loss, 'r', label='Perdida Validacion')
    plt.title('Perdida Entrenamiento y validacion')
    plt.legend()
    plt.show()
'''    
def frecuency_data(data):
    frecuency_nd = []
    frecuencies = TfidfVectorizer()
    frecuencies.fit(data['tweet'])
    for d in data['tweet']:
        frecuency_nd.append(frecuencies.transform([d]))
    frecuencies_np = np.array(frecuency_nd)
    return frecuencies_np
'''

def classify_model(data):
    print("\n\n-----°.°-----°.°-----°.°-----\n")
    
    print(data.columns)
    #manual split data
    #train_dt = data.sample(frac=0.70, random_state=0)
    #test_dt = data.drop(train_dt.index)    
    
    
    tweets = data['tweet'].values
    y = data['polaridad'].values
    
    data_train, data_test, y_train, y_test = train_test_split(
            tweets, y, test_size=0.3, random_state=1000)
    
    x_train = vectorize_data(data_train).transform(data_train)
    x_test = vectorize_data(data_train).transform(data_test)
    
    input_dim = x_train.shape[1]
    
    model = Sequential()
    model.add(layers.Dense(15, input_dim=input_dim, activation="relu"))
    model.add(layers.Dense(12, activation="selu"))
    model.add(layers.Dense(7, activation="sigmoid"))
    model.add(layers.Dense(1, activation="softmax"))
    model.add(layers.Dense(1, activation="softplus"))
    
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics = ['accuracy'])
    model.summary()
    
    history = model.fit(x_train, y_train,
                        epochs=70, verbose=False, validation_data=(x_test, y_test),
                        batch_size=20)
    
    plot_history(history)
    
    loss, accuracy = model.evaluate(x_train, y_train, verbose=False)
    print("Precisión del Modelo con datos de Entrenamiento: {:.4f}".format(accuracy))
    loss, accuracy = model.evaluate(x_test, y_test, verbose=False)
    print("Precisión del Modelo con datos de Prueba:  {:.4f}".format(accuracy))

    y_pred = model.predict(x_test, batch_size=20, verbose=0, steps=None, callbacks=None)    
    
    y_test_tmp = []
    y_pred_tmp = []
    
    for item in range (0, y_test.shape[0]):
        y_test_tmp.append(y_test[item])
        y_pred_tmp.append(y_pred[item])
        
    print('\n')
    print(len(y_test_tmp))
    print("----------------------\n\n")
    print(len(y_pred_tmp))
    
    #confusionMatrix()
    
    '''
    x_train_data = train_dt.drop(['tweet', 'Polaridad'], axis=1)
    cols = x_train_data.columns.tolist()
    cols = cols[-1:]+ cols[:-1]
    x_train_data = x_train_data[cols]
    
    y_train_data = train_dt.drop(['tweet', 'Polaridad', 'vectorized', 'polaridad'], axis=1)
    x_test_data = test_dt.drop(['tweet', 'Polaridad', 'polaridad'], axis=1)
    y_test_data = test_dt.drop(['tweet', 'Polaridad', 'vectorized'], axis=1)
    
    print(x_train_data.shape)
    print(y_train_data.shape)
    print(x_test_data.shape)
    print(y_test_data.shape)
    print("\n")
    #bowTrain, vectorizer = representationBow(x_train_data
    print("\n")
    #print(y_train_data.dtypes)
    print(pd.Series(x_train_data))
    
    cs = [0.05, 0.1, 0.25, 0.5, 0.75, 1, 10, 100]
    for c in cs:
        clf = LogisticRegression(C=c)
        #clf.fit( ,y_train_data)
    
    #Entrenamiento SVM
    kernels=["rbf", "linear"]
    cs = [1, 10, 100, 1000]

    for kernel in kernels:
        for c in cs:
            clf = SVC(kernel=kernel, C=c)
            clf.fit(x_train_data, y_train_data)
            print("%s \t %d \t %.3f" %(kernel[0:3], c, clf.score(x_test_data, y_test_data)))
    #log_model = LogisticRegression().fit(X=x_train, y=y_train)
    
    #y_pred = log_model.predict(x_test)
    '''

dt = load_data()
classify_model(dt)